export default {
    passToClient: ['pageProps', 'urlPathname'],
    hooksTimeout: {
        data: {
            warning: 10 * 1000,
            error: 60 * 1000
        }
    }
}
